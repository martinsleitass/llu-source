package data.model;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by mleitas on 25.02.2015..
 */
@Entity
@Table(name = "orders")
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Temporal(TemporalType.TIMESTAMP)
    private Date created;
    @Temporal(TemporalType.TIMESTAMP)
    private Date shipped;
    @Temporal(TemporalType.TIMESTAMP)
    private Date delivered;

    @OneToMany(mappedBy = "order")
    private List<OrderedProduct> productList;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getShipped() {
        return shipped;
    }

    public void setShipped(Date shipped) {
        this.shipped = shipped;
    }

    public Date getDelivered() {
        return delivered;
    }

    public void setDelivered(Date delivered) {
        this.delivered = delivered;
    }

    public List<OrderedProduct> getProductList() {
        return productList;
    }

    public void setProductList(List<OrderedProduct> productList) {
        this.productList = productList;
    }
}
