package data.service;

import data.model.Product;
import data.model.ProductCategory;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by mleitas on 25.02.2015..
 */
@Stateless
public class DataService {
    @PersistenceContext(name = "shop-pu")
    private EntityManager entityManager;

    public List<ProductCategory> getAllProductCategories(){
        Query query = entityManager.createQuery("select pc from ProductCategory pc");
        return query.getResultList();
    }

    public ProductCategory findCategoryById(int id){
        return entityManager.find(ProductCategory.class, id);
    }

    public ProductCategory saveProductCategory(ProductCategory category){
        if(category.getId() > 0){
            return entityManager.merge(category);
        } else {
            entityManager.persist(category);
        }
        return category;
    }

    public void deleteProductCategory(ProductCategory category){
        category = entityManager.merge(category);
        entityManager.remove(category);
    }

    public List<Product> getAllProducts(){
        Query query = entityManager.createQuery("select p from Product p");
        return query.getResultList();
    }

    public Product saveProduct(Product product){
        //reattach product category to transaction context
        ProductCategory category = entityManager.merge(product.getCategory());
        product.setCategory(category);
        if(product.getId() > 0){
            return entityManager.merge(product);
        } else {
            entityManager.persist(product);
        }
        return product;
    }

    public void deleteProduct(Product product){
        product = entityManager.merge(product);
        entityManager.remove(product);
    }
}
