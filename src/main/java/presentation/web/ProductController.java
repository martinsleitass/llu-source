package presentation.web;

import data.model.Product;
import data.model.ProductCategory;
import data.service.DataService;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@ManagedBean
@ViewScoped
public class ProductController implements Serializable{
    private Product product = new Product();
    private List<Product> products;
    private boolean edit;
    private Integer selectedCategory;

    private SelectItem[] categories;

    @Inject
    private DataService dataService;

    @PostConstruct
    public void init(){
        products = dataService.getAllProducts();
        //odd way to populate dropdown menu for categories. Using "primitive" datatypes only;
        List<ProductCategory> categoryList = dataService.getAllProductCategories();
        categories = new SelectItem[categoryList.size()];
        int i = 0;
        //categories[i++] = new SelectItem(new Integer(-1), "---");
        for(ProductCategory category : categoryList){
            categories[i++] = new SelectItem(new Integer(category.getId()), category.getName());
        }
    }

    public void editProduct(Product product){
        this.product = product;
        this.edit = true;
        for(SelectItem item: categories){
            if(item.getValue().equals((Integer) product.getCategory().getId())){
                this.selectedCategory = (Integer) item.getValue();
                break;
            }
        }
    }

    public void save(){
        ProductCategory category = dataService.findCategoryById(selectedCategory);
        product.setCategory(category);
        dataService.saveProduct(product);
        if(!edit){
            products.add(product);
        }
        this.edit = false;
        this.product = new Product();
        this.selectedCategory = new Integer(-1);
    }

    public void delete(Product product){
        dataService.deleteProduct(product);
        products.remove(product);
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public boolean isEdit() {
        return edit;
    }

    public void setEdit(boolean edit) {
        this.edit = edit;
    }

    public DataService getDataService() {
        return dataService;
    }

    public void setDataService(DataService dataService) {
        this.dataService = dataService;
    }

    public Integer getSelectedCategory() {
        return selectedCategory;
    }

    public void setSelectedCategory(Integer selectedCategory) {
        this.selectedCategory = selectedCategory;
    }

    public SelectItem[] getCategories() {
        return categories;
    }

    public void setCategories(SelectItem[] categories) {
        this.categories = categories;
    }
}
