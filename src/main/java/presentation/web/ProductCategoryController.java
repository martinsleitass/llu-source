package presentation.web;

import data.model.ProductCategory;
import data.service.DataService;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.List;

@ManagedBean
@ViewScoped
public class ProductCategoryController implements Serializable{
    private boolean edit;
    private ProductCategory productCategory = new ProductCategory();
    private List<ProductCategory> categoryList;

    @Inject
    private DataService dataService;

    @PostConstruct
    public void init(){
        categoryList = dataService.getAllProductCategories();
    }

    public void editCategory(ProductCategory category){
        this.productCategory = category;
        this.edit = true;
    }

    public void save(){
        dataService.saveProductCategory(productCategory);
        if(!this.edit){
            this.categoryList.add(productCategory);
        }
        this.edit = false;
        this.productCategory = new ProductCategory();
    }

    public void delete(ProductCategory category){
        this.categoryList.remove(category);
        dataService.deleteProductCategory(category);
    }

    public boolean isEdit() {
        return edit;
    }

    public void setEdit(boolean edit) {
        this.edit = edit;
    }

    public ProductCategory getProductCategory() {
        return productCategory;
    }

    public void setProductCategory(ProductCategory productCategory) {
        this.productCategory = productCategory;
    }

    public List<ProductCategory> getCategoryList() {
        return categoryList;
    }

    public void setCategoryList(List<ProductCategory> categoryList) {
        this.categoryList = categoryList;
    }
}
