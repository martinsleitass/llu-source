package presentation.web.converters;

import data.model.ProductCategory;
import data.service.DataService;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

/**
 * Created by mleitas on 27.02.2015..
 */
@FacesConverter(value = "categoryConverter")
public class CategoryConverter implements Converter{
    @Inject
    private DataService dataService;

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        try {
            String categoryId = value.substring(value.lastIndexOf('(') + 1, value.lastIndexOf(')'));
            return dataService.findCategoryById(Integer.valueOf(categoryId).intValue());
        } catch (Exception ex){
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        try {
            return value.toString();
        } catch (Exception ex){
            ex.printStackTrace();
        }
        return "null value in converter";
    }
}
